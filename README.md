# HPCToolkit Linters

A small repository of small, custom linting passes for use in the HPCToolkit
project. Written entirely in Python with no external dependencies outside of
the PyPI.

### Use with [pre-commit](https://pre-commit.com)
Many linters can also be used as `pre-commit` hooks:
```yaml
- repo: https://gitlab.com/hpctoolkit/linters
  rev: v0.0.0  # Use the ref you want to point at
  hooks:
  - id: header-discipline
```

## Linters

### `header-discipline`
Helper for ensuring that C/C++ headers follow proper header discipline:

> Every header must be self-contained and `#include`-able with no prerequisites.

The easiest way to enforce this is to compile one or more "implementor" source
file(s) for each header that `#include`s it before any others. This linter
identifies and errors in cases where these files are either incorrect or
do not exist.

The relationship between a header and its "implementor" needs to be obvious,
especially for mechanically-formatted code where `#include`s are sorted. This
linter supports the relationship imposed by `clang-format` and is able to
partially read [`.clang-format`](https://clang.llvm.org/docs/ClangFormatStyleOptions.html)
files for configuration. This linter thus imposes the following strict
requirements on the location of a header's "implementor":
 - The "implementor(s)" must be in the same directory as the header they
   implement, and must include it via a local `#include "..."`.
   This is not yet configurable.

 - The "implementor(s)" must `#include` the header they implement before any
   other code (except for one-line `#define` and `#pragma` statements). Passing
   `--allow-first-regex` (or if `IncludeCategories:Priority < 0` in
   `.clang-format`) relaxes this restriction and allows `#include`s matching the
   given regex to appear before the implemented header. This can be important
   for instance to set up [Valgrind annotation macros](https://valgrind.org/docs/manual/drd-manual.html#drd-manual.CXX11).

 - The "implementor(s)" must have an extension corresponding to the header's:
   `.hpp` must be "implemented" by a `.cpp` and `.h` can be implemented by
   either a `.c` or a `.cpp`.
   This is not yet configurable.

 - The "implementor(s)" must have the same stem (basename minus extension) as
   the header they implements, possibly with an additional suffix matching a
   regex provided by `--suffix-regex` (or `IncludeIsMainRegex` in `.clang-format`).
   For example, passing `--suffix-regex '(_test)?$` would consider both `a.c`
   and `a_test.c` as "implementors" of `a.h`.

Note that this linter does not check that the "implementor" is actually
compiled, that is far more complex and outside the scope of this tool.
