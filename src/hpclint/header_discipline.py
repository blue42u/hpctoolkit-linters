#!/usr/bin/env python3

import argparse
import sys
import re
from pathlib import PurePosixPath, Path
import ruamel.yaml
import functools
import itertools
import termcolor
import collections

__main__ = ["main"]

yaml = ruamel.yaml.YAML(typ="safe")


class _CCommentRemover:
    """Stateful line-by-line parser to elide comments and whitespace from C/C++
    code. Does not work perfectly but works well enough for this linter."""

    __re_comment = re.compile(r"/(\*|/)")

    def __init__(self):
        self.in_comment = False

    def __call__(self, code):
        fragments = []
        pos = 0
        if self.in_comment:
            # If we're already in a multi-line comment, look for the end marker
            pos = code.find("*/")
            if pos == -1:
                return ""
            pos += len("*/")
            self.in_comment = False
            if pos >= len(code):
                return ""

        match = self.__class__.__re_comment.search(code[pos:])
        if not match:
            # If there's no comments in this bit of code, no need to do anything
            return code[pos:]
        while match:
            fragments.append(code[pos : pos + match.start()])
            pos += match.end()
            if match.group() == "//":
                # Comment extends to the end of the line or string
                pos = code.find("\n", pos)
                if pos == -1:
                    break
            elif match.group() == "/*":
                # Comment extends until the */ marker
                pos = code.find("*/", pos)
                if pos == -1:
                    self.in_comment = True
                    break
                pos += len("*/")
            else:
                assert False, f"Bad match: {match}"

            match = self.__class__.__re_comment.search(code[pos:])

        return "".join(fragments)


@functools.lru_cache
def _get_clang_format(dirp):
    for trial in itertools.chain([dirp], dirp.parents):
        trial = trial / ".clang-format"
        if trial.is_file():
            with open(trial, "r") as tf:
                # Scan for matching clauses and overlay the ones that match
                return collections.ChainMap(
                    *[
                        doc
                        for doc in yaml.load_all(tf)
                        if "Language" not in doc or doc["Language"] == "Cpp"
                    ]
                )
    return None


def _get_suffix(hdr):
    data = _get_clang_format(hdr.parent)
    if data is None:
        return None
    return data.get("IncludeIsMainRegex")


_re_compile = functools.lru_cache(re.compile)

_re_include = re.compile(r'^\s*#include\s+("[^"]+"|<[^>]+>)')


def _get_allow_first(hdr):
    data = _get_clang_format(hdr.parent)
    if data is None:
        return None
    sortinc = data.get("SortIncludes", "Never")
    if sortinc == "Never":
        return None

    flags = 0
    if sortinc == "CaseInsensitive":
        flags |= re.IGNORECASE

    fragments = []
    for cat in data.get("IncludeCategories", []):
        if "Regex" in cat and int(cat.get("Priority", 1000)) < 0:
            fragments.append(cat["Regex"])
    if fragments:
        try:
            return re.compile("|".join(fragments), flags=flags)
        except Exception:
            pass
    return None


def main(arguments=None):
    parser = argparse.ArgumentParser(description="Verify C/C++ headers follow header discipline")
    parser.add_argument("headers", nargs="+", type=Path, help="Headers to check for discipline")
    parser.add_argument(
        "--allow-first-regex",
        help='Regex of #includes to allow before the main #include in a source file (includes the <> or "")',
        type=re.compile,
    )
    parser.add_argument(
        "--suffix-regex",
        help="Regex of allowed suffixes on the headers stem to derive the source file",
    )
    args = parser.parse_args(arguments)
    del parser

    exts_map = {
        ".hpp": (".cpp",),
        ".h": (".c", ".cpp"),
    }

    failures = 0
    for hdr in args.headers:
        if hdr.suffix not in exts_map:
            print(
                termcolor.colored(f"{hdr}", attrs=["bold"])
                + f": Unrecognized extension {hdr.suffix}"
            )
            failures += 1
            continue
        exts = exts_map[hdr.suffix]
        suffix_re = _re_compile(args.suffix_regex or _get_suffix(hdr) or "$")

        # Scan for any trials, in a rather arbitrary order
        implementors = 0
        for trial in hdr.parent.iterdir():
            if not trial.stem.startswith(hdr.stem) or not suffix_re.match(
                trial.stem[len(hdr.stem) :]
            ):
                continue
            if trial.suffix not in exts:
                continue
            expected = f'"{hdr.name}"'

            implementors += 1
            with open(trial, "r") as tf:
                # Parse through any comments until we get to the first #include
                lineno = 0
                for line in map(_CCommentRemover(), tf):
                    lineno += 1
                    inc = _re_include.match(line)
                    if inc:
                        # First #include located, let's see if it's reasonable
                        if inc.group(1) != expected:
                            # Failure! Check if it falls under --allow-first-regex
                            allow_first = args.allow_first_regex or _get_allow_first(hdr)
                            if allow_first and allow_first.search(inc.group(1)):
                                # Still ok, continue to next line
                                continue
                            # Print a message detailing the failure
                            print(
                                termcolor.colored(f"{trial}:{lineno:d}", attrs=["bold"])
                                + f": First includes `{inc.group(1)}', expected `{expected}'"
                            )
                            failures += 1

                        # We found what we want, no need to continue
                        break

                    # If we hit real code before the first #include, that's bad
                    if line and not line.isspace():
                        lline = line.lstrip()

                        # Special case: #pragmas are allowed before #includes
                        if lline.startswith("#pragma"):
                            continue
                        # Special case: #define of the SOURCE constants is allowed before #includes
                        if re.match("^#define\s+_\w+_SOURCE(\s|$)", lline):
                            continue

                        # Print a message detailing the failure
                        print(
                            termcolor.colored(f"{trial}:{lineno:d}", attrs=["bold"])
                            + f": Live code before first include, expected `#include {expected}'"
                        )
                        failures += 1
                        break

        if implementors == 0:
            print(termcolor.colored(f"{hdr}", attrs=["bold"]) + f": No matching source file found")
            failures += 1

    return 0 if failures == 0 else 1
